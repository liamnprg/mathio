extern crate tokio;
extern crate rayon;
#[macro_use] extern crate futures;
extern crate serde;
extern crate serde_json;
#[macro_use] extern crate serde_derive;
extern crate rusttorial;
extern crate num_bigint;
extern crate bytes;

use rusttorial::*;

use tokio::io;
use tokio::net::TcpListener;
use tokio::net::TcpStream;
use tokio::prelude::*;
use num_bigint::BigUint;
use bytes::{BytesMut, Bytes, BufMut};

#[derive(Debug)]
struct Lines {
    /// The TCP socket.
    socket: TcpStream,

    /// Buffer used when reading from the socket. Data is not returned from this
    /// buffer until an entire line has been read.
    rd: BytesMut,

    /// Buffer used to stage data before writing it to the socket.
    wr: BytesMut,
}


impl Lines {
    /// Create a new `Lines` codec backed by the socket
    fn new(socket: TcpStream) -> Self {
        Lines {
            socket,
            rd: BytesMut::new(),
            wr: BytesMut::new(),
        }
    }

    /// Buffer a line.
    ///
    /// This writes the line to an internal buffer. Calls to `poll_flush` will
    /// attempt to flush this buffer to the socket.
    fn buffer(&mut self, line: &[u8]) {
        // Ensure the buffer has capacity. Ideally this would not be unbounded,
        // but to keep the example simple, we will not limit this.
        self.wr.reserve(line.len());

        // Push the line onto the end of the write buffer.
        //
        // The `put` function is from the `BufMut` trait.
        self.wr.put(line);
    }

    /// Flush the write buffer to the socket
    fn poll_flush(&mut self) -> Poll<(), io::Error> {
        // As long as there is buffered data to write, try to write it.
        while !self.wr.is_empty() {
            // Try to write some bytes to the socket
            let n = try_ready!(self.socket.poll_write(&self.wr));

            // As long as the wr is not empty, a successful write should
            // never write 0 bytes.
            assert!(n > 0);

            // This discards the first `n` bytes of the buffer.
            let _ = self.wr.split_to(n);
        }

        Ok(Async::Ready(()))
    }

    /// Read data from the socket.
    ///
    /// This only returns `Ready` when the socket has closed.
    fn fill_read_buf(&mut self) -> Poll<(), io::Error> {
        loop {
            // Ensure the read buffer has capacity.
            //
            // This might result in an internal allocation.
            self.rd.reserve(1024);

            // Read data into the buffer.
            let n = try_ready!(self.socket.read_buf(&mut self.rd));

            if n == 0 {
                return Ok(Async::Ready(()));
            }
        }
    }
}

impl Stream for Lines {
    type Item = BytesMut;
    type Error = io::Error;

    fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
        // First, read any new data that might have been received off the socket
        let sock_closed = self.fill_read_buf()?.is_ready();

        // Now, try finding lines
        let pos = self.rd.windows(2).enumerate()
            .find(|&(_, bytes)| bytes == b"\r\n")
            .map(|(i, _)| i);

        if let Some(pos) = pos {
            // Remove the line from the read buffer and set it to `line`.
            let mut line = self.rd.split_to(pos + 2);

            // Drop the trailing \r\n
            line.split_off(pos);

            // Return the line
            return Ok(Async::Ready(Some(line)));
        }

        if sock_closed {
            Ok(Async::Ready(None))
        } else {
            Ok(Async::NotReady)
        }
    }
}

#[derive(Deserialize, Debug)]
struct Command {
    func: String,
    args: Vec<u64>,
}

fn geta(v: Vec<u64>) -> u64 {
    if v.len() == 1 {
        return v[0]
    }
    0
}
fn geta2(v: Vec<u64>) -> (u64,u64) {
    if v.len() == 2 {
        return (v[0],v[1])
    }
    (0,0)
}

impl Command {
    fn run(self) -> BigUint {
        match self.func.as_str() {
            "c" => { 
                let (o,t) = geta2(self.args);

                c(o,t).unwrap()
            },
            "f" => {
                let o = geta(self.args);
                factorial(o)
            }
            "fn" => {
                let (o,t) = geta2(self.args);
                factorialn(o,t)
            }
            "a" => {
                let o = geta(self.args);
                plustorial(o)
            }
            "an" => {
                let (o,t) = geta2(self.args);
                plustorialn(o,t)
            }
            _ => {
                BigUint::from(0 as u64)
            }
        }
    }
}


fn main() {
    let addr = "127.0.0.1:6142".parse().unwrap();
    let listener = TcpListener::bind(&addr).unwrap();
    let server = listener.incoming().for_each(|socket| {
    println!("accepted socket; addr={:?}", socket.peer_addr().unwrap());

/*        io::write_all(write, "hello world\n")
            .then(|res| {
                println!("wrote message; success={:?}", res.is_ok());
                Ok(())
            });*/
//        let buf: Vec<u8> = vec!();
         
        let lines = Lines::new(socket);
        let reader = lines.into_future()
            .and_then(|res| {
                println!("got message");
                let (data,mut stream) = res;
                handle(data.unwrap());
                stream.buffer(b"hello world\n");
                loop {
                    match stream.poll_flush() {
                        Ok(Async::Ready(_)) => return Ok(()),
                        Ok(Async::NotReady) => continue,
                        Err(e) => {
                            panic!("{}",e)
                        },
                    }
                }
            }).map_err(|e| {
                println!("{}",e.0);
                ()
            });
        tokio::spawn(reader);
        Ok(())
    })
    .map_err(|err| {
        println!("accept error = {:?}", err);
    });

    println!("server running on localhost:6142");

    tokio::run(server);
}

fn handle(d: bytes::BytesMut) {
    let cmd: Command = serde_json::from_slice(&d.freeze()[..]).unwrap();
    println!("Func: {}",cmd.func);
    println!("{}",cmd.run())
}
